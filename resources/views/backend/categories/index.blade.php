@extends('backend.layouts.master');

@section('content')
            <div class="container mt-4">

                @if (session('success'))
                        {{-- {{session('success')}} --}}
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Well Done!</strong> {{session('success')}}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                          </div>
                @endif

                @if (session('errors'))
                        {{-- {{session('success')}} --}}
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>Sorry!</strong> {{session('errors')}}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                          </div>
                @endif

                <h6 class="text-center mb-3">Category Lists :</h6>
                <div class="card">
                    <div class="card-body">
                        <a href="{{route('category.create')}}" class="btn btn-primary btn-md mb-3">Add New Category</a>
                        <table class="table table-bordered text-center">

                            <thead>
                                <tr>
                                    <th>Ser No</th>
                                    <th>Category Name</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                                
                            </thead>

                            <tbody>

                                @foreach ($data as $category)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$category->name}}</td>
                                            <td class="text-center">
                                                <a href="" class="btn btn-sm btn-primary">Show</a>
                                                <a class="btn btn-sm btn-success" href="{{route('category.edit', $category->id)}}">Edit</a>
                                                <a href="" class="btn btn-sm btn-danger">Delete</a>
                                            </td>
                                        </tr>
                                @endforeach
                                    
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
@endsection