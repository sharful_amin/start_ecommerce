@extends('backend.layouts.master');

@section('content')

<a href="{{route('category.index')}}" class="btn btn-primary btn-sm mt-3">List</a>

            <div class="card">

                <div class="card-body">
                        <form action="{{route('category.store')}}" method="POST">

                            @csrf
        
                            <label for="category_name">Category Name :</label>
                            <input 
                                type="text"
                                name="name"
                                placeholder="Enter your category name"
                                class="form-control"
                                value="{{old('name')}}"
                            >

                            @error('name')
                                        <span class="text-danger">{{$message}}</span>
                            @enderror
        
                            <div>
                                <button type="submit" class="btn btn-sm btn-primary mt-3">Save</button>
                            </div>
                        </form>
                </div>
                
            </div>
@endsection