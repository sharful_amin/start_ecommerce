<!DOCTYPE html>
<html lang="en">

{{-- head including --}}
@include('backend.layouts.partials.head')
{{-- head including --}}

<body>

    {{-- sidebar including --}}
    @include('backend.layouts.partials.aside')
    {{-- sidebar including --}}

    {{-- navbar including --}}
    @include('backend.layouts.partials.navbar')
    {{-- navbar including --}}

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                
                @yield('content')

                {{-- footer including --}}
                @include('backend.layouts.partials.footer')
                {{-- footer including --}}
            </div>
        </div>
    </div>

    

    <!-- jquery vendor -->
    <script src="{{asset('ui/backend')}}/js/lib/jquery.min.js"></script>
    <script src="{{asset('ui/backend')}}/js/lib/jquery.nanoscroller.min.js"></script>
    <!-- nano scroller -->
    <script src="{{asset('ui/backend')}}/js/lib/menubar/sidebar.js"></script>
    <script src="{{asset('ui/backend')}}/js/lib/preloader/pace.min.js"></script>
    <!-- sidebar -->

    <script src="{{asset('ui/backend')}}/js/lib/bootstrap.min.js"></script>
    <script src="{{asset('ui/backend')}}/js/scripts.js"></script>
    <!-- bootstrap -->

    <script src="{{asset('ui/backend')}}/js/lib/calendar-2/moment.latest.min.js"></script>
    <script src="{{asset('ui/backend')}}/js/lib/calendar-2/pignose.calendar.min.js"></script>
    <script src="{{asset('ui/backend')}}/js/lib/calendar-2/pignose.init.js"></script>


    <script src="{{asset('ui/backend')}}/js/lib/weather/jquery.simpleWeather.min.js"></script>
    <script src="{{asset('ui/backend')}}/js/lib/weather/weather-init.js"></script>
    <script src="{{asset('ui/backend')}}/js/lib/circle-progress/circle-progress.min.js"></script>
    <script src="{{asset('ui/backend')}}/js/lib/circle-progress/circle-progress-init.js"></script>
    <script src="{{asset('ui/backend')}}/js/lib/chartist/chartist.min.js"></script>
    <script src="{{asset('ui/backend')}}/js/lib/sparklinechart/jquery.sparkline.min.js"></script>
    <script src="{{asset('ui/backend')}}/js/lib/sparklinechart/sparkline.init.js"></script>
    <script src="{{asset('ui/backend')}}/js/lib/owl-carousel/owl.carousel.min.js"></script>
    <script src="{{asset('ui/backend')}}/js/lib/owl-carousel/owl.carousel-init.js"></script>
    <!-- scripit init-->
    <script src="{{asset('ui/backend')}}/js/dashboard2.js"></script>
</body>

</html>