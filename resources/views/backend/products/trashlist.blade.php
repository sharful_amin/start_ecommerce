@extends('backend.layouts.master')

@section('content')

{{-- @dd($products); --}}

            @if (session('message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                             <strong>Well Done!</strong> {{session('message')}}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
            @endif

            @if (session('errors'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                             <strong>Sorry!</strong> {{session('errors')}}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
            @endif

            <div class="container">
                <h6 class="text-center mt-3">Product List :</h6>
                <a class="btn btn-sm btn-primary mb-3" href="{{route('product.create')}}">Add New Product</a>
                <button class="btn btn-sm btn-secondary mb-3">Excel</button>

                <a class="btn btn-sm btn-success mb-3" href="{{route('product.pdf')}}">PDF</a>

                <button class="btn btn-sm btn-warning mb-3">Trash Bin</button>
                            <table class="table table-bordered text-center">
                                <thead>
                                <tr>
                                    <th scope="col">Ser No</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Price</th>
                                    <th scope="col" class="text-center">Actions</th>
                                </tr>
                                </thead>


                                <tbody>
                                
                                @foreach ($products as $data)
                                        <tr>
                                            <th scope="row">{{$loop->iteration}}</th>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->price}}</td>
                                            <td class="text-center">
                                                <button class="btn btn-sm btn-primary">Show</button>
                                                <a class="btn btn-sm btn-success" href="{{route('product.edit', $data->id)}}">Edit</a>

                                                <form action="{{route('product.destroy', $data->id)}}" method="POST" style="display:inline">

                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" onclick="return confirm('Are you sure want to delete ?')" class="btn btn-sm btn-danger" >Delete</button>

                                                </form>
                                                
                                            </td>
                                        </tr>
                                @endforeach

                                </tbody>
                            </table>
            </div>

@endsection