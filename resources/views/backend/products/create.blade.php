@extends('backend.layouts.master');

@section('content')

            <div class="container mt-5">
                <form action="{{route('product.store')}}" method="POST">

                    @csrf

                    <div class="mb-3">
                      <label for="name" class="form-label">Product Name :</label>
                      <input 
                        type="text" 
                        class="form-control" 
                        id="name"
                        name="name"
                        placeholder="Please Enter Product Name"
                        value="{{old('name')}}"
                        >

                    @error('name')
                           <span class="text-danger">{{$message}}</span>
                    @enderror


                    </div>

                    <div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                  </form>
            </div>

@endsection