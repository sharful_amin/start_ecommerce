<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\PdfController;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/admin', function () {
//     return view('backend.index');
// });

Route::get('/admin', [AdminController::class, 'index'])->name('admin.dashboard');

Route::prefix('product')->group(function(){

    Route::get('/product', [ProductController::class, 'product'])->name('admin.product');
    Route::get('/', [ProductController::class, 'index'])->name('product.index');
    Route::get('/create', [ProductController::class, 'create'])->name('product.create');
    Route::post('/store', [ProductController::class, 'store'])->name('product.store');
    Route::get('/edit/{id}', [ProductController::class, 'edit'])->name('product.edit');
    Route::post('/update/{id}', [ProductController::class, 'update'])->name('product.update');
    Route::get('/product-pdf', [PdfController::class, 'downloadProductData'])->name('product.pdf');
    Route::delete('/delete/{id}', [ProductController::class, 'destroy'])->name('product.destroy');

});

Route::prefix('category')->group(function(){
    Route::get('/', [CategoryController::class, 'index'])->name('category.index');
    Route::get('/create', [CategoryController::class, 'create'])->name('category.create');
    Route::post('/store', [CategoryController::class, 'store'])->name('category.store');
    Route::get('/edit/{id}', [CategoryController::class, 'edit'])->name('category.edit');
    Route::post('/update/{id}', [CategoryController::class, 'update'])->name('category.update');
    Route::delete('/delete/{id}', [CategoryController::class, 'destroy'])->name('category.destroy');

    
    // Trash Items Routes
    Route::get('/trashlist', [CategoryController::class, 'trashlist'])->name('category.trashlist');
    Route::get('/restore/{id}', [CategoryController::class, 'restoreItem'])->name('category.restore');
    Route::get('/force_delete/{id}', [CategoryController::class, 'delete'])->name('category.delete');
    
});


