<?php

namespace App\Http\Controllers;

use App\Models\Product;

use Illuminate\Http\Request;


class PdfController extends Controller
{
    public function downloadProductData(){
        // dd("check");
        $products = Product::all();
        // dd($data);

        // $categories = Category::all();
        $fileName = 'product.pdf';
        $html = view('backend.products.product_pdf', compact('products'))->render();
        $mpdf = new \Mpdf\Mpdf();
        //  $mpdf->addPage("L");
        $mpdf->WriteHTML($html);
        $mpdf->Output($fileName,'I');
    }
}


