<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Exception;
use App\Http\Requests\CategoryRequest;

class CategoryController extends Controller
{
    public function index(){
        $data = Category::all();
        return view('backend.categories.index', compact('data'));
    }

    public function create(){
        return view('backend.categories.create');
    }

    public function store(CategoryRequest $request){

            // $request->validate([
            //     'name' => 'required'
            // ]);

            // $request->validate([
            //     'name' => 'required|unique:categories'
            // ]);

            

        try{
            // dd($request->all());
            // dd($request->all());
            $data = $request->all();

            Category::create($data);
            return redirect()->route('category.index')->withSuccess('Category Added Successfully !');
        }
        catch(Exception $e){
            // dd($e->getMessage());
            // return redirect()->route('category.index')->withErrors($e->getMessage());
            // return redirect()->back()->withErrors($e->getMessage());
            return redirect()->route('category.index')->withErrors($e->getMessage());
        }
    }

    public function edit($id){
        // dd("check");
        // dd($id);
        $category = Category::where('id', $id) -> first();
        // dd($category);
        return view('backend.products.edit', compact('category'));
    }

    public function update(CategoryRequest $request, $id){
        try{
            // dd($request->all());
            // $data = $request->all();
            $category = $request->except('_token');

            Category::where('id', $id)->update($category);
            return redirect()->route('category.index')->withMessage('Product Updated Successfully Done !');
        }
        catch(Exception $e){
            return redirect()->route('category.index')->withErrors($e->getMessage());
        }
    }
}
