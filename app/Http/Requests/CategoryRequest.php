<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    
    public function authorize()
    {
        // return false;
        return true;
    }

    
    public function rules()
    {
        return [
            'name' => 'required|unique:categories'
        ];
    }
}
